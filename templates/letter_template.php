<div style="margin: 0px; padding: 0px; float: left; width: 100%; font-family: 'Segoe UI', Verdana, Tahoma, Arial, Helvetica, sans-serif; font-size: 12pt;">
	<div style="margin: 50px auto; width: 640px;">
		<div style="padding: 20px 30px; background: #f5f5f5;">
			<span style="display: block;"><span style="font-weight: bold;">Name:</span> [name]</span>
			<span style="display: block;"><span style="font-weight: bold;">E-mail:</span> [email]</span>
			<span style="display: block;"><span style="font-weight: bold;">Phone:</span> [phone]</span>
			<span style="display: block;"><span style="font-weight: bold;">Subject:</span> [subject]</span>
			<span style="display: block;"><span style="font-weight: bold;">Message:</span></span>
			<p style="margin: 0px;">[message]</p>
		</div>
	</div>
</div>