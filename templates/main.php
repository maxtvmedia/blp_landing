<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="ru">
	<head>

		<title><?php echo $SEO->title; ?></title>
		<meta name="description" content="<?php echo $SEO->description; ?>">
		<meta name="keywords" content="<?php echo $SEO->keywords; ?>">

		<meta name="MobileOptimized" content="width">
		<meta name="HandheldFriendly" content="true">
		<meta name="viewport" content="width=320, user-scalable=no">
		
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
		
		<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>/system/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>/system/bootstrap/css/bootstrap-datetimepicker.css" />
		<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>/system/bootstrap/css/bootstrap-switch.css" />
		<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>/system/bootstrap/css/bootstrap.colorpickersliders.css" />
		
		<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>/system/css/jquery-ui-1.10.1.custom.css" />
		
		<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>/system/fonts/iconfont/material-icons.css" />
		<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>/system/fonts/fontello/fontello.css" />

		<link rel="stylesheet" href="<?=BASE_URL?>/system/sky-carousel/default_skin.css" />
		
		<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>/system/css/core.css" />
		
		<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>/system/css/style.css" />

		<script type="text/javascript" src="<?=BASE_URL?>/system/js/jquery.js"></script>
		<script type="text/javascript" src="<?=BASE_URL?>/system/js/device.js"></script>

		<script type="text/javascript" src="<?=BASE_URL?>/system/js/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="<?=BASE_URL?>/system/js/jquery-ui-1.10.1.custom.js"></script>
		<script type="text/javascript" src="<?=BASE_URL?>/system/js/tinycolor.min.js"></script>
		
		<script type="text/javascript" src="<?=BASE_URL?>/system/bootstrap/js/bootstrap.js"></script>
		<script type="text/javascript" src="<?=BASE_URL?>/system/bootstrap/js/bootstrap-datetimepicker.js"></script>
		<script type="text/javascript" src="<?=BASE_URL?>/system/bootstrap/js/bootstrap-switch.js"></script>
		<script type="text/javascript" src="<?=BASE_URL?>/system/bootstrap/js/bootstrap.colorpickersliders.js"></script>
		
		<script type="text/javascript" src="<?=BASE_URL?>/system/tinymce/tinymce.min.js"></script>
		
		<script type="text/javascript" src="<?=BASE_URL?>/system/sky-carousel/jquery.sky.carousel-1.0.2.min.js"></script>
		
		<script type="text/javascript" src="<?=BASE_URL?>/system/js/jquery.core.js"></script>
		
		<script type="text/javascript" src="<?=BASE_URL?>/system/js/script.js"></script>
		<script type="text/javascript" src="<?=BASE_URL?>/api/site_menu/js/script.js"></script>
		<script type="text/javascript" src="<?=BASE_URL?>/api/site_logo/js/script.js"></script>
		<script type="text/javascript" src="<?=BASE_URL?>/api/site_header/js/script.js"></script>
		
		<script type="text/javascript" src="<?=BASE_URL?>/system/tinymce/plugins/moxiemanager/js/moxman.loader.min.js"></script>
		
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2hInIREhT03eIwu6k25zXdgWL2IK8Zwc&signed_in=true&libraries=places"></script>
		
		
	</head>

	<body>

		<?php
			require_once BASE_PATH.'/api/api_color.php';
		?>
		<!--
		<div class="search-wrap">
			<span class="material-icons search-close">close</span>
			
			<div class="main-container search">
				<input type="text" name="search" class="input-field search" placeholder="Search...">
				<span class="material-icons search-button">search</span>
			</div>
		</div>
		-->
		
		<?php
			require_once BASE_PATH.'/api/site_header/view.php';
		?>

		<div class="page_material <?=(isset($H_POSITION) && $H_POSITION>0) ? 'position' : ''; ?>" material="site_menu" template="view_content">
		<?php
			require_once BASE_PATH.'/api/site_menu/view_content.php';
		?>
		</div>
		
		<div id="map" class="main-wrap map">
		<?php
			require_once BASE_PATH.'/api/site_map/view.php';
		?>
		</div>
		
		<div class="main-wrap footer">
			<div class="main-container">
				<ul class="footer-links">
					<li><a href="#">Home</a></li>
					<li><a href="#">Privacy</a></li>
					<li><a href="#">Terms of Use</a></li>
				</ul>
				
				<span class="footer-copyright">© 2016 All Rights Reserved</span>
			</div>
		</div>

		<div id="preload"></div>
<script type="text/javascript">
	jQuery().ready(function(){
		jQuery( document )
			.ajaxSend(function() {
				jQuery("body").append("<div id='ajaxpreloder' style='z-index:10000;position:fixed;left:0px;top:0px;height:100%;width:100%;background:white url(images/loading.gif) center no-repeat;opacity:0.7;text-align:center'></div>");
			})
			.ajaxError(function(){
				
					jQuery("#ajaxpreloder").remove();
				
			})
			.ajaxSuccess(function(){
				
					jQuery("#ajaxpreloder").remove();
				
			})
			.ajaxComplete(function(){
				
					jQuery("#ajaxpreloder").remove();
				
			});
	});
	
</script>
	</body>
</html>