<script type="text/javascript" src="<?=BASE_URL?>/api/site_map/js/script.js"></script>

<div class="map-wrap">
	<?php
		$sql=mysql_query("select * from maxtv_sites where id=$SITE_ID");
		$row=mysql_fetch_assoc($sql);
		
		$map_activate=$row['map_activate'];
		$map_address=$row['address'];
		$address_lat=$row['address_lat'];
		$address_lng=$row['address_lng'];

		if ($USER->role=='admin' || $map_activate>0)
		{
			if ($USER->role=='admin')
			{
			echo "<div class='settings_menu absolute invisible right'>";
				echo "<div class='button_small rec' onclick=dialog_form('site_contacts','address',null,null,map_obj); title='Edit'></div>";
				echo "<input type=checkbox name=map_activate class=map-toggle ".($map_activate>0 ? 'checked' : '').">";
			echo "</div>";
			}
			
			print "<div id=\"map-content\"></div>";
		}
	?>
	<script type="text/javascript">
	var myLatLng=
	{
		lat: <?=is_numeric($address_lat)?$address_lat:0; ?>,
		lng: <?=is_numeric($address_lng)?$address_lng:0; ?>
	}

	var map = new google.maps.Map(document.getElementById('map-content'),
	{
		center: myLatLng,
		scrollwheel: false,
		zoom: 18
	});

	var marker = new google.maps.Marker(
	{
		position: myLatLng,
		map: map,
		title: '<?php echo $map_address; ?>'
	});

	var contentString = '<div id="content">'+
		'<?php echo $map_address; ?>'+
	'</div>';

	var infowindow = new google.maps.InfoWindow(
	{
		content: contentString
	});

	infowindow.open(map, marker);
	marker.addListener('click', function()
	{
		infowindow.open(map, marker);
	});
	</script>
</div>