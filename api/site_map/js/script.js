$(function()
{
	$('input.map-toggle').bootstrapSwitch(
	{
		size: 'mini'
	});
	
	$('input.map-toggle').on('switchChange.bootstrapSwitch', function(event, state)
	{
		$.ajax(
		{
			type: "POST",
			url: "api/api_load.php",
			data: 
			{
				'load_material': 'site_contacts',
				'load_template': 'upload',
				'event': $(this).attr('name')
			}
		});
	});

	//initMap();
});

function initMap()
{
	var map = new google.maps.Map(document.getElementById('map-frame'),
	{
		center: 
		{
			lat: -34.397,
			lng: 150.644
		},
		scrollwheel: false,
		zoom: 8
	});
}

map_obj=
{
	'on_form_load': false,
	'load_material': 'site_map',
	'load_template': 'view',
	'target': 'div#map'
}