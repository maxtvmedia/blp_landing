$(function()
{
	$('input.contacts-toggle').bootstrapSwitch(
	{
		size: 'mini'
	});
	
	$('input.contacts-toggle').on('switchChange.bootstrapSwitch', function(event, state)
	{
		$.ajax(
		{
			type: "POST",
			url: "api/api_load.php",
			data: 
			{
				'load_material': 'site_contacts',
				'load_template': 'upload',
				'event': $(this).attr('name')
			}
		});
	});
	
	$('button.submit').click(function(event)
	{
		event.preventDefault();
		
		if (validate_message())
		{
			form=$('form.contacts-form').serializeArray();
			form.push({name: 'load_material', value: 'site_contacts'});
			form.push({name: 'load_template', value: 'upload'});
			form.push({name: 'event', value: 'submit_letter'});
			
			$.ajax(
			{
				type: "POST",
				url: "/api/api_load.php",
				data: form,
				
				success: function(msg)
				{
					obj=jQuery.parseJSON(msg);
					$('form.contacts-form').find('input[type=text], textarea').val('');
					message_box('accept', obj.message);
				}
			});
		}
	});
});


contacts_obj=
{
	'on_form_load': false,
	'load_material': 'site_contacts',
	'load_template': 'view',
	'target': 'div.content-wrap.contacts'
}


function validate_message()
{
	form=$('form.contacts-form').serializeArray();
		
	pattern_count=new RegExp('^[0-9]+$','i');
	pattern_price=new RegExp('^[0-9.]+$','i');
	pattern_tel=new RegExp('^[0-9()+\\s-]+$','i');
	pattern_mail=new RegExp('^[0-9a-z_\\-\\.]+@[0-9a-z_\\-\\.]+$','i');
	pattern_login=new RegExp('^[0-9a-z]+$','i');
	pattern_url=new RegExp('^[0-9a-z_\\-]+$','i');
	pattern_dir=new RegExp('^[0-9a-z_\\-/]+$','i');
	pattern_unlock_data=new RegExp('^[0-9]{15}$','i');

	all_valid=true;
	jQuery.each(form, function(i, field)
	{
	is_valid=true;

		switch (field.name)
		{
			case 'name':
			case 'subject':
			case 'message':
				if (field.value.length==0)
				is_valid=false;
			break;
			
			case 'phone':
				is_valid=pattern_tel.test(field.value);
			break;

			case 'mail':
				is_valid=pattern_mail.test(field.value);
			break;
		}

		if (is_valid==true)
			$('form.contacts-form [name='+field.name+']').removeClass('error');
		else
		{
			$('form.contacts-form [name='+field.name+']').addClass('error');
				
			all_valid=false;
		}
	});

return all_valid;
}
