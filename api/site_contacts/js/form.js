$(function()
{
	init_input();
});

function init_input()
{
	input = document.getElementById('address-input');
	searchBox = new google.maps.places.SearchBox(input);
	
	searchBox.addListener('places_changed', function()
	{
		var places = searchBox.getPlaces();
		lat=places[0].geometry.location.lat();
		lng=places[0].geometry.location.lng();
		
		$('input#lat').val(lat);
		$('input#lng').val(lng);
	});
}
