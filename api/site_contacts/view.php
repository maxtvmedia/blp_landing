<script type="text/javascript" src="<?=BASE_URL?>/api/site_contacts/js/script.js"></script>

<ul class="contacts-list">
	<?php
		$sql=mysql_query("select * from maxtv_sites where id=$SITE_ID");
		$row=mysql_fetch_assoc($sql);
		
		$phone=str_replace(',', '<br>', str_replace(' ', '', $row['phone']));
		$email=explode(',', str_replace(' ', '', $row['email']));
		$address=$row['address'];
		$phone_activate=$row['phone_activate'];
		$email_activate=$row['email_activate'];
		$address_activate=$row['address_activate'];
		$form_activate=$row['form_activate'];

		if ($USER->role=='admin' || $address_activate>0)
		{
		echo "<li>";
			if ($USER->role=='admin')
			{
			echo "<div class='settings_menu absolute invisible right'>";
				echo "<div class='button_small rec' onclick=dialog_form('site_contacts','address',null,null,contacts_obj); title='Edit'></div>";
				echo "<input type=checkbox name=address_activate class=contacts-toggle ".($address_activate>0 ? 'checked' : '').">";
			echo "</div>";
			}
			
			echo "<span class=material-icons>location_on</span>";
			echo "<span class=contact-label>";
				echo "<span>$address</span>";
				echo "<a class=map-link href=#map>View Map</a>";
			echo "</span>";
		echo "</li>";
		}
		
		if ($USER->role=='admin' || $phone_activate>0)
		{
		echo "<li>";
			if ($USER->role=='admin')
			{
			echo "<div class='settings_menu absolute invisible right'>";
				echo "<div class='button_small rec' onclick=dialog_form('site_contacts','phone',null,null,contacts_obj); title='Edit'></div>";
				echo "<input type=checkbox name=phone_activate class=contacts-toggle ".($phone_activate>0 ? 'checked' : '').">";
			echo "</div>";
			}
			
			echo "<span class=material-icons>phone</span>";
			echo "<span class=contact-label>$phone</span>";
		echo "</li>";
		}
		
		if ($USER->role=='admin' || $email_activate>0)
		{
		echo "<li>";
			if ($USER->role=='admin')
			{
			echo "<div class='settings_menu absolute invisible right'>";
				echo "<div class='button_small rec' onclick=dialog_form('site_contacts','mail',null,null,contacts_obj); title='Edit'></div>";
				echo "<input type=checkbox name=email_activate class=contacts-toggle ".($email_activate>0 ? 'checked' : '').">";
			echo "</div>";
			}
			
			echo "<span class=material-icons>email</span>";
			
			echo "<span class=contact-label>";
				foreach ($email as $mail)
					print "<span><a href=mailto:$mail>$mail</a></span>";
			echo "</span>";
		echo "</li>";
		}
	?>
</ul>

<?php
if ($USER->role=='admin' || $form_activate>0)
{
?>
<form class="contacts-form">
	<?php
		if ($USER->role=='admin')
		{
		echo "<div class='settings_menu absolute invisible right'>";
			echo "<input type=checkbox name=form_activate class=contacts-toggle ".($form_activate>0 ? 'checked' : '').">";
		echo "</div>";
		}
	?>
	
	<div class="field-wrap left">
		<input type="text" name="name" placeholder="Name">
	</div>
	
	<div class="field-wrap right">
		<input type="text" name="mail" placeholder="Email">
	</div>
		
	<div class="field-wrap left">
		<input type="text" name="phone" placeholder="Phone">
	</div>
		
	<div class="field-wrap right">
		<input type="text" name="subject" placeholder="Subject">
	</div>
		
	<div class="field-wrap center">
		<textarea name="message" placeholder="Message"></textarea>
	</div>
	
	<button class="submit">Send Message</button>
</form>
<?php
}
?>
