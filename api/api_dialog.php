<?php
class api_dialog
{
	function dialog($title, $form, $width, $vartical, $position = 0)
	{
	global $PREFIX;
	
		$title=($title!=false ? $title : 'Delete');
		$form=($form!=false ? $form : 'Delete?');
		$width=($width!=false ? $width : 700);
		$vartical=($vartical!=false ? $vartical : 'top');
		$dialog_id=time();
		
		$html="<div class=modal-dialog>";
			$html.="<div class=modal-content>";
				$html.="<div class=modal-header>";
					$html.="<button class=close data-dismiss=modal aria-label=Close><span aria-hidden=true>clear</span></button>";
					$html.="<h4 class=modal-title>$title</h4>";
					
					if (!$position)
					{
					$html.="<div class=buttons-wrap>";
						$html.="<button id=modal-submit dialog_id=$dialog_id class='btn btn-default'>Ok</button>";
						$html.="<button id=modal-cancel class='btn btn-primary' data-dismiss=modal>Cancel</button>";
					$html.="</div>";
					}
					
				$html.="</div>";
				
				$html.="<div class=modal-body>";
					$html.=$form;
				$html.="</div>";
				
				if ($position)
				{
				$html.="<div class=modal-footer>";
					$html.="<button id=modal-submit dialog_id=$dialog_id class='btn btn-default'>Ok</button>";
					$html.="<button id=modal-cancel class='btn btn-primary' data-dismiss=modal>Cancel</button>";
				$html.="</div>";
				}
					
			$html.="</div>";
		$html.="</div>";
		
		$dialog['lang']=$PREFIX;
		$dialog['width']=$width;
		$dialog['html']=$html;
		$dialog['dialog_id']=$dialog_id;
		
		print json_encode($dialog);
	}

	function dialogGallery($title, $form, $width, $vartical){
        $title=($title!=false ? $title : 'Delete');
        $form=($form!=false ? $form : 'Delete?');
        $width=($width!=false ? $width : 700);

        $html="<div class=modal-dialog>
            <div class=modal-content>
                <div class=modal-header>
                    <button class=close data-dismiss=modal aria-label=Close><span aria-hidden=true>clear</span></button>
                    <h4 class=modal-title>$title</h4>
                </div>
                <div class=modal-body>$form</div>
            </div>
        </div>";
        $dialog['lang'] = null;
        $dialog['width'] = $width;
        $dialog['html'] = $html;
        $dialog['dialog_id']= time();

        print json_encode($dialog);
    }
}
?>