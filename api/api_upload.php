<?php
function obj_image($old_size_w, $old_size_h, $new_site_w, $new_size_h, $image)
{
	$new_width=$old_size_w;
	$new_height=$old_size_h;

	if ($old_size_w>$new_site_w)
	{
	$new_height=ceil($new_site_w*$old_size_h/$old_size_w);
	$new_width=$new_site_w;
	
		if ($new_height>$new_size_h)
		{
			$new_width=ceil($new_size_h*$new_width/$new_height);
			$new_height=$new_size_h;
		}
	}
	else if ($old_size_h>$new_size_h)
	{
	$new_width=ceil($new_size_h*$old_size_w/$old_size_h);
	$new_height=$new_size_h;
		
		if ($new_width>$new_site_w)
		{
			$new_height=ceil($new_site_w*$new_height/$new_width);
			$new_width=$new_site_w;
		}
	}
	
	$new_image=imagecreatetruecolor($new_width,$new_height);
	$white=imagecolorallocatealpha($new_image, 0, 0, 0, 127);
	imagefill($new_image,0,0,$white);
	imagesavealpha($new_image, true);
	imagecopyresampled($new_image,$image,0,0,0,0,$new_width,$new_height,$old_size_w,$old_size_h);
	
	return $new_image;
}




function upload_file($field_id, $file)
{
	if (!isset($file))
	$file=$_FILES['upload_file']['tmp_name'][0];
	
	if ($file)
	{
		$file_settings=getimagesize($file);

		$old_width=$file_settings[0];
		$old_height=$file_settings[1];
		
		switch ($file_settings[2])
		{
			case 1:
				$old_image=imagecreatefromgif($file);
			break;
			
			case 2:
				$old_image=imagecreatefromjpeg($file);
			break;
			
			case 3:
				$old_image=imagecreatefrompng($file);
			break;
			
			default:
				$old_image=imagecreatefromjpeg($file);
		}
		
		switch ($_POST['load_material'])
		{
			case 'site_menu':
				$dir='/images/background/';
				$width=3000;
				$height=2000;
				$extension='jpg';
				$table='maxtv_sites_menu';
				$field='id';
				$pic_name='bg-'.$field_id.'.'.$extension;
			break;
			
			case 'site_logo':
				$dir='/images/';
				$width=200;
				$height=40;
				$extension='png';
				$table='maxtv_sites';
				$field='id';
				$pic_name='logo.'.$extension;
			break;
		}
		
		$pic_path=BASE_PATH.$dir.$pic_name;
		
		$new_image=obj_image($old_width, $old_height, $width, $height, $old_image);
		
		switch ($extension)
		{
			case 'png':
				imagepng($new_image, $pic_path);
			break;
			
			case 'jpg':
				imagejpeg($new_image, $pic_path);
			break;
		}
		imagedestroy($old_image);
		imagedestroy($new_image);
		
		$hash=md5(rand(1,1000));
		mysql_query("update $table set pic_path='$pic_name', hash='$hash' where($field=$field_id)");
	}
}
?>
