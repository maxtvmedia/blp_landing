<?php
function unique_url($table, $field, $name)
{
	$sql=mysql_query("select $field from $table");
	while ($row = mysql_fetch_array($sql))
		$urls[]=$row[$field];
	
	$converter = array(
	'а' => 'a',   'б' => 'b',   'в' => 'v',
	'г' => 'g',   'д' => 'd',   'е' => 'e',
	'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
	'и' => 'i',   'й' => 'y',   'к' => 'k',
	'л' => 'l',   'м' => 'm',   'н' => 'n',
	'о' => 'o',   'п' => 'p',   'р' => 'r',
	'с' => 's',   'т' => 't',   'у' => 'u',
	'ф' => 'f',   'х' => 'h',   'ц' => 'c',
	'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
	'ь' => '',    'ы' => 'y',   'ъ' => '',
	'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
	' ' => '-');
	
	$url=preg_replace('/[^a-zа-я0-9_\s\-]/ui', '', mb_strtolower($name, 'UTF-8'));
	$url=strtr($url, $converter);
	
	if (!in_array($url, $urls))
		$new_url=$url;
	else
	{
		$i=1;
		while ($is_url!==false)
		{
			$new_url=$url.'-'.$i;
			$is_url=array_search($new_url, $urls);

			$i++;
		}
	}
	
	return $new_url;
}


function get_tree($table, $key, $field, $id)
{
global $TREE;

	$sql=mysql_query("select $key from $table where $field=$id");
	while ($row=mysql_fetch_assoc($sql))
	{
		$TREE[]=$row[$key];
		
		get_tree($table, $key, $field, $row[$key]);
	}
}

function get_breadcrambs($table, $key, $field, $id)
{
global $PREFIX;
global $BREADCRAMBS;

	$sql=mysql_query("select $key, ".$PREFIX."_name as name, url from $table where $field=$id");
	while ($row=mysql_fetch_assoc($sql))
	{
		$BREADCRAMBS[]=array(
			'url'	=> $row['url'],
			'name'	=> $row['name']
		);
		
		get_breadcrambs($table, $key, $field, $row[$key]);
	}
}

foreach($_POST as $key => $value)
{
	switch ($key)
	{
		case 'load_obj':
			$_POST[$key] = json_decode($value);
		break;
		
		case 'message':
			$_POST[$key] = nl2br($value);
		break;
		
		default:
			if (!is_array($value))
			$_POST[$key] = mysql_real_escape_string($value);
	}
}
?>