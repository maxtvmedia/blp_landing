<script type="text/javascript" src="<?=BASE_URL?>/api/site_menu/js/content.js"></script>

<?php
$sql=mysql_query("select * from maxtv_sites where id=$SITE_ID");
$row=mysql_fetch_assoc($sql);
$map_activate=$row['map_activate'];
$map_address=$row['address'];
$address_lat=$row['address_lat'];
$address_lng=$row['address_lng'];
$street_view = $row['street_view'];
if(!empty($street_view)){
	$street_view = json_decode($street_view);
}
$sql=mysql_query("select * from maxtv_sites_menu where activate=1 && site_id=$SITE_ID order by priority");
while ($row=mysql_fetch_assoc($sql))
{

	$menu_id=$row['id'];
	$menu_bg=$row['pic_path'] ? "style='background-image: url(".$row['pic_path'].");'" : '';
	$menu_content=$row['content'];
	$menu_block_id=$row['block_id'];
	$menu_activate=$row['activate'];
	$c_title=$row['c_title'];
	$ct_color=$row['ct_color'];
	
	echo "<div id='$menu_block_id' class=main-wrap $menu_bg>";
		switch ($menu_block_id)
		{
			case 'home':
				echo "<div class=slide-wrap>";
					echo "<div class=slide>";
						echo "<h1 class=slide-title><span style='color: $ct_color;'>$c_title</span></h1>";
						echo $menu_content;
					echo "</div>";
				echo "</div>";
			break;
			
			case 'gallery':
				if ($USER->role=='admin')
				{
					echo "<div class='settings_menu absolute invisible right'>";
						echo "<div class='button_small add' onclick=dialog_form('site_gallery','add',null,null,menu_obj); title='Add'></div>";
						echo "<div class='button_small rec' onclick=dialog_form('site_menu','rec','$menu_id',null,menu_obj); title='Edit'></div>";
						//echo "<div class='button_small del' onclick=dialog_form('site_menu','del','$menu_id',null,menu_obj); title='Del'></div>";
						echo "<input type=checkbox name=block-toggle id=$menu_id class=block-toggle ".($menu_activate>0 ? 'checked' : '').">";
					echo "</div>";
				}
				
				echo "<div class=gallery-wrap>";
					echo $c_title ? "<h2 class=content-title style='color: ".($ct_color ? $ct_color : '#fff').";'>$c_title</h2>" : "";
					require_once BASE_PATH.'/api/site_gallery/view.php';
				echo "</div>";
			break;
			
			case 'amenities':

				if ($USER->role=='admin')
				{
					echo "<div class='settings_menu absolute invisible right'>";
						echo "<div class='button_small rec' onclick=dialog_form('site_menu','rec','$menu_id',null,menu_obj); title='Edit'></div>";
						//echo "<div class='button_small del' onclick=dialog_form('site_menu','del','$menu_id',null,menu_obj); title='Del'></div>";
						echo "<input type=checkbox name=block-toggle id=$menu_id class=block-toggle ".($menu_activate>0 ? 'checked' : '').">";
					echo "</div>";
				}
				
				echo "<div class=content-wrap>";
					echo "<h2 class=content-title style='color: ".($ct_color ? $ct_color : '#fff').";'>$c_title</h2>";
					echo $menu_content;
					require_once BASE_PATH.'/api/site_amenties/view.php';
				echo "</div>";
			break;

			case 'contact-us':
				if ($USER->role=='admin')
				{
					echo "<div class='settings_menu absolute invisible right'>";
						echo "<div class='button_small rec' onclick=dialog_form('site_menu','rec','$menu_id',null,menu_obj); title='Edit'></div>";
						//echo "<div class='button_small del' onclick=dialog_form('site_menu','del','$menu_id',null,menu_obj); title='Del'></div>";
						echo "<input type=checkbox name=block-toggle id=$menu_id class=block-toggle ".($menu_activate>0 ? 'checked' : '').">";
					echo "</div>";
				}
				
				echo "<div class=content-wrap>";
					echo "<h2 class=content-title style='color: ".($ct_color ? $ct_color : '#fff').";'>$c_title</h2>";
					echo $menu_content;
					require_once BASE_PATH.'/api/site_contacts/view.php';
				echo "</div>";
			break;
			
			case 'neighbourhood':
				if ($USER->role=='admin')
				{
					echo "<div class='settings_menu absolute invisible right'>";
						echo "<div class='button_small rec' onclick=dialog_form('site_menu','rec','$menu_id',null,menu_obj); title='Edit'></div>";
						//echo "<div class='button_small del' onclick=dialog_form('site_menu','del','$menu_id',null,menu_obj); title='Del'></div>";
						echo "<input type=checkbox name=block-toggle id=$menu_id class=block-toggle ".($menu_activate>0 ? 'checked' : '').">";
					echo "</div>";
				}
				
				echo "<div class=main-container>";
					echo "<div class=desc-container>";
						echo "<div class=desc-bg></div>";
						echo "<div class=desc-wrap>";
							echo "<h3 class=desc-title>$c_title</h3>";
							echo $menu_content;
						echo "</div>";
					echo "</div>";
					
					echo "<div class=clear></div>";
				echo "</div>";
				
				echo "<div id=panorama></div>";
				echo "<div class=panorama-activation></div>";
			break;
			
			default:
				if ($USER->role=='admin')
				{
					echo "<div class='settings_menu absolute invisible right'>";
						echo "<div class='button_small rec' onclick=dialog_form('site_menu','rec','$menu_id',null,menu_obj); title='Edit'></div>";
						//echo "<div class='button_small del' onclick=dialog_form('site_menu','del','$menu_id',null,menu_obj); title='Del'></div>";
						echo "<input type=checkbox name=block-toggle id=$menu_id class=block-toggle ".($menu_activate>0 ? 'checked' : '').">";
					echo "</div>";
				}
				
				echo "<div class=content-wrap>";
					echo "<h2 class=content-title style='color: ".($ct_color ? $ct_color : '#fff').";'>$c_title</h2>";
					echo $menu_content;
				echo "</div>";
		}
	echo "</div>";
}
?>

<script>
	var fenway = 
	{
		lat: <?=(isset($street_view) && isset($street_view->lat))?$street_view->lat:(is_numeric($address_lat)?$address_lat:0);?>, 
		lng: <?=(isset($street_view) && isset($street_view->lng))?$street_view->lng:(is_numeric($address_lng)?$address_lng:0);?>
	};
  
	var panorama = new google.maps.StreetViewPanorama(
	document.getElementById('panorama'),
	{
		position: fenway,
		pov:
		{
			heading: <?=isset($street_view) && isset($street_view->heading)?$street_view->heading:38;?>,
			pitch: <?=isset($street_view) && isset($street_view->pitch)?$street_view->pitch:10;?>,
			zoom:  <?=isset($street_view) && isset($street_view->zoom)?$street_view->zoom:1;?>
		},
		addressControl: false
	});
<?
if ($USER->role=='admin'){
?>
	var StreetView = {
		heading: 0,
		pitch: 0,
		lat: 0,
		lng: 0,
		zoom: 0
	};	

	var streetViewTimeout = false;

	panorama.addListener('pov_changed', function(){
		/*jQuery("#street_view_heading-input").val(panorama.getPov().heading);
		jQuery("#street_view_pitch-input").val(panorama.getPov().pitch);
		jQuery("#street_view_lat-input").val(panorama.getPosition().lat());
		jQuery("#street_view_lng-input").val(panorama.getPosition().lng());
		*/
		StreetView.heading = panorama.getPov().heading;
		StreetView.pitch = panorama.getPov().pitch;
		StreetView.lat = panorama.getPosition().lat();
		StreetView.lng = panorama.getPosition().lng();
		StreetView.zoom = panorama.getPov().zoom; 
		clearTimeout(streetViewTimeout);
		(function(StreetView){streetViewTimeout = setTimeout(function(){SaveStreetView(StreetView);},2000);})(StreetView);
	});

	function SaveStreetView(street_view){
			console.log("SAVE");
			street_view.act = "street_view";
			jQuery.ajax({
				url: 'api/direct_save.php',	
				type: 'post',
				data: street_view
			});
	}
<?
}
?>

	$('div.panorama-activation').click(function()
	{
		$(this).hide();
	});
</script>
