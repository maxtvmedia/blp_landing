$(function()
{
	$('a.hlink').click(function()
	{
		position=$('div.main-wrap'+$(this).attr('href')).offset();
		$('body').animate({scrollTop: position.top}, '300');
	});
	
	$('a.map-link').click(function()
	{
		position=$('div.main-wrap'+$(this).attr('href')).offset();
		$('body').animate({scrollTop: position.top}, '300');
	});
	
	$('a.sign-in').click(function(event)
	{
		event.preventDefault();
		
		$.ajax(
		{
			type: "POST",
			url: "api/api_load.php",
			data: 
			{
				'load_material': 'site_user',
				'load_template': 'upload',
				'event': 'sign_in'
			},
			success: function(msg)
			{
				obj=jQuery.parseJSON(msg);
				window.location = obj.link;
			}
		});
	});
});

menu_obj=
{
	'on_form_load': false,
	'load_material': 'site_menu',
	'load_template': 'view',
	'target': 'div.menu-wrap',
	'load_page': function()
	{
		load_page();
	}
}