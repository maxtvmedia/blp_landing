$(function()
{
	$('div#sky-carousel').carousel(
	{
		itemWidth: device.mobile() ? 300 : 730, // The width of your images.
		itemHeight: device.mobile() ? 300 : 470, // The height of your images.
		loop: false,
		gradientOverlayVisible: false,
		unselectedItemAlpha: 0.4,
		preload: false,
		topMargin: 0,
		enableMouseWheel: false
	});
});
