$(function()
{
	$('ul.main-menu.active').sortable(
	{
		items: 'li.menu-link',
		
		stop: function(event, ui)
		{
			result=$(this).sortable('serialize', {key: 'data[]'})+'&load_material=site_menu&load_template=upload&event=sort';
			
			$.ajax(
			{
				type: "POST",
				url: "api/api_load.php",
				data: result,
				success: function()
				{
					load_page();
				}
			});
		}
	});
	
	$('ul.main-menu li').hover(
	function()
	{
		$(this).find('a.hlink').css(
		{
			'color': $(this).find('a.hlink').attr('hover-color')
		});
	},
	function()
	{
		$(this).find('a.hlink').css(
		{
			'color': $(this).find('a.hlink').attr('color')
		});
	});
});