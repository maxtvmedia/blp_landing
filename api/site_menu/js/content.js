$(function()
{
	$('input.block-toggle').bootstrapSwitch(
	{
		size: 'mini'
	});

	$('input.block-toggle').on('switchChange.bootstrapSwitch', function(event, state)
	{
		$.ajax(
		{
			type: "POST",
			url: "api/api_load.php",
			data:
			{
				'load_material': 'site_menu',
				'load_template': 'upload',
				'event': 'toggle',
				'id': $(this).attr('id')
			},
			success: function()
			{
				on_load(menu_line);
				//load_page();
			}
		});
	});
});

menu_line=
{
	'on_form_load': false,
	'load_material': 'site_menu',
	'load_template': 'view',
	'target': 'div.menu-wrap'
};