<?php

require_once 'init.php';

if(!isset($ROUTER)){
	$ROUTER = new stdClass();
}

$ROUTER->material=isset($_POST['load_material'])?$_POST['load_material']:'';
$ROUTER->template=isset($_POST['load_template'])?$_POST['load_template']:'';
if(!empty($ROUTER->material) && !empty($ROUTER->template) && file_exists(BASE_PATH."/api/$ROUTER->material/$ROUTER->template.php")){
	require_once BASE_PATH."/api/$ROUTER->material/$ROUTER->template.php";
}
?>