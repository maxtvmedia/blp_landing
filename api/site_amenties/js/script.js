$(function()
{
	$('input.amenties-toggle').bootstrapSwitch(
	{
		size: 'mini'
	});
	
	$('input.amenties-toggle').on('switchChange.bootstrapSwitch', function(event, state)
	{
		$.ajax(
		{
			type: "POST",
			url: "/api/api_load.php",
			data: 
			{
				'load_material': 'site_amenties',
				'load_template': 'upload',
				'event': 'amenities_view',
				'id': $(this).attr('id'),
				'value': state ? 1 : 0
			},
			success: function(msg)
			{
				$(event.currentTarget).closest('li').toggleClass('activate');
			}
		});
	});
});