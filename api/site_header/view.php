<?php
$sql=mysql_query("select * from maxtv_sites where id=$SITE_ID");
$row=mysql_fetch_assoc($sql);
$h_position=$row['h_position'];
$h_color=$h_position>0 ? 'transparent' : $row['h_color'];
?>

<div class="main-wrap head <?php echo $h_position>0 ? 'position' : ''; ?>" <?php echo $h_color ? "style='background: $h_color;'" : ""; ?>>
	<?php
	if ($USER->role=='admin')
	{
		echo "<div class='settings_menu absolute invisible right'>";
			echo "<div class='button_small rec' onclick=dialog_form('site_header','rec',null,null,header_obj); title='Edit'></div>";
		echo "</div>";
	}
	?>
	
	<div class="main-container head-desctop">
		<div class="logo-wrap">
			<?php
				require_once BASE_PATH.'/api/site_logo/view.php';
			?>
		</div>
		<!--<a class="search-toggle" href="#"><span class="material-icons">search</span></a>-->
		
		<div class="menu-wrap">
		<?php
			require_once BASE_PATH.'/api/site_menu/view.php';
		?>
		</div>
	</div>
	
	<div class="main-container head-mobile">
		<span class="mobile-menu-button material-icons">menu</span>
		
		<?php
			require_once BASE_PATH.'/api/site_logo/view_mobile.php';
		?>
		
		<?php
			require_once BASE_PATH.'/api/site_menu/view_mobile.php';
		?>
	</div>
</div>
