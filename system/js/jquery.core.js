$(function()
{
});

function message_box(class_name, content, time_out)
{
	$('#message_box').remove();
	
	$('<div/>',
	{
		id: 'message_box',
		class: class_name,
		html: content
	}).appendTo('body');
	
	$('#message_box').fadeIn(500).delay(time_out ? time_out : 4000).fadeOut(500, function()
	{
		$(this).remove();
	});
}



function tyny_activate(lang)
{
	$(document).on('focusin', function(e)
	{
		e.stopImmediatePropagation();
	});
	
	if ($('textarea').hasClass('simple'))
	tinymce.init(
	{
		selector: 'textarea',
		toolbar_items_size: 'small',
		height : 150,
		language : lang,
		relative_urls : true,
		statusbar : false,
		menubar: false,
        toolbar1: 'styleselect | bold italic underline strikethrough | alignleft aligncenter alignright | bullist numlist ',
	});
	else
	tinymce.init(
	{
		selector: 'textarea[name="string"]',
		toolbar_items_size: 'small',
		height : 300,
		language : lang,
		relative_urls: true,
		remove_script_host: false,
		//valid_elements: '*[*]',
		statusbar : false,
		menubar: false,
        plugins: [
                'advlist autolink link image imagetools lists charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                'table contextmenu directionality emoticons template textcolor paste textcolor moxiemanager'
        ],
		
        toolbar1: 'bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect',
        toolbar2: 'cut copy paste | bullist numlist | undo redo | link unlink image media code | inserttime | forecolor backcolor'
	});
}

function tyny_disable(window_id)
{
	if (tinymce.activeEditor)
	{
		modal_id=$(tinymce.activeEditor.targetElm).closest('div.modal-window').attr('timestamp');
		
		if (modal_id==window_id)
			tinymce.activeEditor.remove();
	}
}


function dialog_form(material, event, id, template, callback)
{
	$.ajax(
	{
		url: 'api/api_load.php',
		type: 'POST',
		
		data: 
		{
			'load_material': material,
			'load_template': 'form',
			'event': event,
			'id': id
		},
		
		success: function(msg)
		{
			if(!msg) {
				return false;
			}
			obj=jQuery.parseJSON(msg);

			modal_window=$('<div/>',
			{
				id: 'dialog_'+obj.dialog_id,
				class: 'modal fade modal-window',
				timestamp: obj.dialog_id,
				role: 'dialog',
				tabindex: -1,
				html: obj.html
			}).appendTo('body');

			if (callback)
			{
				callback.id=$(callback.target).attr('id');
				$('body').data(''+obj.dialog_id, callback);
				
				if (callback.on_form_load)
				on_load(callback);
			}
			
			$('div.form_tabs').tabs(
			{
				create: function(event, ui)
				{
					if (ui.tab.attr('event'))
					{
						callback=
						{
							'load_material': material,
							'load_template': 'on_load',
							'event': ui.tab.attr('event'),
							'id': $('div.'+ui.tab.attr('event')).attr('id'),
							'target': 'div.'+ui.tab.attr('event')
						};
						on_load(callback);
					}
				},
				
				activate: function(event, ui)
				{
					if (ui.newTab.attr('event'))
					{
						callback=
						{
							'load_material': material,
							'load_template': 'on_load',
							'event': ui.newTab.attr('event'),
							'id': $('div.'+ui.newTab.attr('event')).attr('id'),
							'target': 'div.'+ui.newTab.attr('event')
						};
						on_load(callback);
					}
				}
			});
			
			$('div.form_accordion').accordion(
			{
				heightStyle: 'content',
				create: function(event, ui)
				{
					if (ui.header.attr('event'))
					{
						callback=
						{
							'load_material': material,
							'load_template': 'on_load',
							'event': ui.header.attr('event'),
							'id': $('div.'+ui.header.attr('event')).attr('id'),
							'target': 'div.'+ui.header.attr('event')
						};
						on_load(callback);
					}
				},
				
				beforeActivate: function(event, ui)
				{
					if (ui.newHeader.attr('event'))
					{
						callback=
						{
							'load_material': material,
							'load_template': 'on_load',
							'event': ui.newHeader.attr('event'),
							'id': $('div.'+ui.newHeader.attr('event')).attr('id'),
							'target': 'div.'+ui.newHeader.attr('event')
						};
						on_load(callback);
					}
				}
			});
			
			$('input.date').keypress(function(event)
			{
				return false;
			});
			
			$('input.color').ColorPickerSliders(
			{
				size: 'sm',
				placement: 'right',
				swatches: false,
				sliders: false,
				hsvpanel: true
			});
			
			$('input.date').datetimepicker(
			{
				format: 'YYYY-MM-DD',
				showTodayButton: true,
				icons:
				{
					today: 'glyphicon glyphicon-calendar'
				},
				locale: obj.lang
			});
			
			$('div#dialog_'+obj.dialog_id).keydown(function(e)
			{
				if (e.keyCode == $.ui.keyCode.ENTER)
				{
					modal_window.find('button#modal-submit').click();
					e.preventDefault();
				}
			});
			
			modal_window.on('show.bs.modal', function(event)
			{
				tyny_activate(obj.lang);
				$(this).find('div.modal-dialog').css(
				{
					width: obj.width
				});
			});
			
			modal_window.modal(
			{
				backdrop: false
			});
			
			modal_window.on('hidden.bs.modal', function(event)
			{
				tyny_disable($(this).attr('timestamp'));
				$(this).remove();
			});
			
			modal_window.find('button#modal-submit').click(function()
			{
				$(this).prop('disabled', true);
				upload_dialog_form($(this).attr('dialog_id'), material, event, id, template);
			});
		}
	});
}


function upload_dialog_form(window_id, material, event, id, template)
{
	callback=$('body').data(''+window_id);
	
	if ($('div#dialog_'+window_id+' textarea').is('textarea'))
	$('div#dialog_'+window_id+' textarea').val(tinyMCE.activeEditor.getContent());
	
	dialog_array=$('div#dialog_'+window_id).find('#dialog_form').serializeArray();
	dialog_array.push({name: 'load_material', value: material});
	dialog_array.push({name: 'load_template', value: 'upload'});
	dialog_array.push({name: 'event', value: event});
	dialog_array.push({name: 'id', value: id});
	dialog_array.push({name: 'file_template', value: template});
	
	formdata = new FormData();
	valid_pic=true;
	if ($('div#dialog_'+window_id+' input').is('#upload_file'))
	{
		upload_file=$('div#dialog_'+window_id+' input#upload_file');
		
		if (upload_file[0].files.length>0)
		{
			$.each(upload_file[0].files, function()
			{
				file=this.name;
				extension = file.substr((file.lastIndexOf('.')+1)).toLowerCase();
				switch (extension)
				{
					case 'jpg':
					case 'jpeg':
					case 'gif':
					case 'png':
						formdata.append('upload_file[]', this);
						upload_file.removeClass('error');
					break;
					
					default:
						upload_file.addClass('error');
						valid_pic=false;
				}
			});
		}
		else if (!upload_file.hasClass('allow'))
		{
			upload_file.addClass('error');
			valid_pic=false;
		}
	}
	
	if (validate_form(window_id, material, event, id, template, dialog_array) && valid_pic)
	{
		$('#preload').show();
		
		$.each(dialog_array, function(key, form_obj)
		{
			formdata.append(form_obj.name, form_obj.value);
		});

		$.ajax(
		{
			type: 'POST',
			url: 'api/api_load.php',
			data: formdata,
			cache: false,
			contentType: false,
			processData: false,
			xhr: function()
			{
				var xhr = new XMLHttpRequest();
				
				xhr.upload.addEventListener('progress', function(event)
				{
					percent = Math.round((event.loaded / event.total) * 100);
					
					//console.log(percent);
					//$('input#upload').progressbar('value', percent);
				}, false);
				return xhr;
			},

			success: function(msg)
			{
				obj=msg.length>2 ? jQuery.parseJSON(msg) : false;
				
				$('#dialog_'+window_id).modal('hide');
				if(material == 'site_amenties' && (event == 'del' || event == 'rec' || event == 'add_images')) {
					$('.modal').modal('hide');
				}
				$('#preload').hide();

				if (callback)
				{
					if (callback.load_obj)
						callback.load_obj(obj);
					else
						on_load(callback);
				}
				else
					load_page();
				
				$('body').removeData(''+window_id);
				
				
				if (obj)
				{
					if (obj.message)
					message_box(obj.message.type, obj.message.text);
					
					if (obj.event)
						if (obj.event.result)
							switch (obj.event.request)
							{
								case 'reload':
									window.location.reload();
								break;
								
								case 'redirect':
									window.location = obj.event.link;
								break;
							}
				}
			}
		});
	}
	else
	$('div#dialog_'+window_id).find('button#modal-submit').prop('disabled', false);
}


function validate_form(window_id, material, event, id, template, form)
{
	pattern_count=new RegExp('^[0-9]+$','i');
	pattern_price=new RegExp('^[0-9.]+$','i');
	pattern_tel=new RegExp('^[0-9()\\s-]+$','i');
	pattern_mail=new RegExp('^[0-9a-z_\\-\\.]+@[0-9a-z_\\-\\.]+$','i');
	pattern_login=new RegExp('^[0-9a-z]+$','i');
	pattern_url=new RegExp('^[0-9a-z_\\-]+$','i');
	pattern_dir=new RegExp('^[0-9a-z_\\-/]+$','i');
	pattern_unlock_data=new RegExp('^[0-9]{15}$','i');

	all_valid=true;
	jQuery.each(form, function(i, field)
	{
	is_valid=true;

		switch (field.name)
		{
			case 'name':
			case 'user_name':
			case 'user_info':
			case 'address':
			case 'date':
			case 'domain':
			case 'region':
			case 'city':
			case 'street':
			case 'zip':
			case 'title':
			case 'extension':
				if (field.value.length==0)
				is_valid=false;
			break;
			
			case 'image':
				is_valid=false;
				
				if (field.value.length>0)
				{
					$.ajax(
					{
						type: 'POST',
						url: 'api/api_validate.php',
						async: false,
						data:
						{
							'url': field.value
						},

						success: function(msg)
						{
							obj=jQuery.parseJSON(msg);
							is_valid=obj.valid;
						}
					});
				}
				else
					if ($('div#dialog_'+window_id+' [name="'+field.name+'"]').hasClass('allow'))
						is_valid=true;
			break;
			
			case 'unlock_data':
				is_valid=pattern_unlock_data.test(field.value);
			break;
			
			case 'count':
			case 'item_count':
			case 'item_id':
			case 'discount':
				is_valid=pattern_count.test(field.value);
			break;

			case 'price':
			case 'percent':
			case 'balance':
			case 'width':
			case 'height':
			case 'markup':
				is_valid=pattern_price.test(field.value);
			break;

			case 'tel':
				is_valid=pattern_tel.test(field.value);
			break;

			case 'mail':
				is_valid=pattern_mail.test(field.value);
			break;
			
			case 'style':
			case 'template':
				is_valid=pattern_url.test(field.value);
			break;
			
			case 'dir':
				is_valid=pattern_dir.test(field.value);
			break;
			
			case 'code':
			case 'password':
			case 'new_password':
				is_valid=pattern_login.test(field.value);
			break;

			case 'image_code':
				begin_code=document.cookie.indexOf('image_code=');
				img_code=document.cookie.substring(begin_code+11, begin_code+17);
				
				if (field.value!=img_code)
				is_valid=false;
			break;
			
			case 'url':
				if ($('div#dialog_'+window_id+' [name="'+field.name+'"]').hasClass('active'))
				{
					pattern=$('div#dialog_'+window_id+' [name="'+field.name+'"]').attr('pattern');
					switch (pattern)
					{
						case 'mail':
							is_valid=pattern_mail.test(field.value);
						break;
						
						case 'password':
							is_valid=pattern_login.test(field.value);
						break;
						
						default:
							is_valid=true;
							if (field.value.length>0)
								is_valid=pattern_url.test(field.value);
					}
					
					if (is_valid)
					{
						$.ajax(
						{
							type: 'POST',
							url: 'api/api_validate.php',
							async: false,
							data:
							{
								'validate': 'on',
								'material': material,
								'template': template,
								'event': event,
								'name': $('div#dialog_'+window_id+' [name="name"]').val(),
								'url': field.value,
								'id': id
							},

							success: function(msg)
							{
								obj=jQuery.parseJSON(msg);
								
								if (obj.link)
									field.value=obj.link;
								
								if (!obj.url)
									is_valid=false;
							}
						});
					}
				}
			break;
		}

		if (is_valid==true)
		{
		$('div#dialog_'+window_id+' [name='+field.name+']').removeClass('error');
		
			if (field.name=='item_id')
			$('div#dialog_'+window_id+' [name=item_name]').removeClass('error');
		}
		else
		{
		$('div#dialog_'+window_id+' [name='+field.name+']').addClass('error');
		
			if (field.name=='item_id')
			$('div#dialog_'+window_id+' [name=item_name]').addClass('error');
			
		all_valid=false;
		}
	});

return all_valid;
}


function load_page()
{
	material=$('div.page_material').attr('material');
	template=$('div.page_material').attr('template');
	
	$.ajax(
	{
		type: 'POST',
		url: 'api/api_load.php',
		data: 
		{
			'load_material': material,
			'load_template': template
		},

		success: function(msg)
		{
			$('div.page_material').html(msg);
		}
	});
}


function on_load(callback){
	$.ajax({
		type: 'POST',
		url: 'api/api_load.php',
		async: callback.async,
		data: callback,
		success: function(msg) {
			switch (callback.on_replace) {
				case 'append':
					$(callback.target).append(msg);
					break;
				default:
					if (callback.on_replace){
						$(callback.target).replaceWith(msg);
					} else {
						$(callback.target).html(msg);
					}
					break;
			}
		}
	});
}


function vk_login()
{
	VK.Auth.login(function (response)
	{
		if (response.session)
		{
		vk_id=response.session.user.id;
		
			$.ajax(
			{
				url: 'api/api_load.php',
				type: 'POST',
				data: 
				{
					'load_material': 'page_user',
					'load_template': 'upload',
					'event': 'sign_in',
					'action': 'vk_login',
					'vk_id': vk_id
				},
				
				success: function(msg)
				{
				obj=jQuery.parseJSON(msg);

					if (obj.event.result)
					window.location.reload();
					else
					message_box(obj.message.type, obj.message.text);
				}
			});
		}
	});
}


function vk_import()
{
	VK.Auth.login(function (response)
	{
		if (response.session)
		{
		vk_id=response.session.user.id;
		
			$.ajax(
			{
				url: 'api/api_load.php',
				type: 'POST',
				data: 
				{
					'load_material': 'page_user',
					'load_template': 'upload',
					'event': 'vk_import',
					'vk_id': vk_id
				},
				
				success: function(msg)
				{
					obj=jQuery.parseJSON(msg);
					message_box(obj.message.type, obj.message.text);
				}
			});
		}
	});
}

function vk_reg()
{
	VK.Auth.login(function (response)
	{
		if (response.session)
		{
		vk_id=response.session.user.id;
		vk_login_name=response.session.user.domain;
		vk_first_name=response.session.user.first_name;
		vk_last_name=response.session.user.last_name;
		
			$.ajax(
			{
				url: 'api/api_load.php',
				type: 'POST',
				data: 
				{
					'load_material': 'page_user',
					'load_template': 'upload',
					'event': 'vk_reg',
					'vk_id': vk_id,
					'vk_login': vk_login_name,
					'vk_first_name': vk_first_name,
					'vk_last_name': vk_last_name
				},
				
				success: function(msg)
				{
					obj=jQuery.parseJSON(msg);
					
					if (obj.login==1)
					vk_login();
					else
					message_box(obj.message.type, obj.message.text);
				}
			});
		}
	});
}



function fb_login()
{

	FB.login(function(response) 
	{
		if (response.authResponse) 
		{
		fb_id=response.authResponse.userID;
	
			$.ajax(
			{
				url: 'api/api_load.php',
				type: 'POST',
				data: 
				{
					'load_material': 'page_user',
					'load_template': 'upload',
					'event': 'sign_in',
					'action': 'fb_login',
					'fb_id': fb_id
				},
				
				success: function(msg)
				{
				obj=jQuery.parseJSON(msg);

					if (obj.event.result)
					window.location.reload();
					else
					message_box(obj.message.type, obj.message.text);
				}
				
			});
		
		}
	});
}



function fb_import()
{
	FB.login(function(response) 
	{
		if (response.authResponse) 
		{
		fb_id=response.authResponse.userID;
	
			$.ajax(
			{
				url: 'api/api_load.php',
				type: 'POST',
				data: 
				{
					'load_material': 'page_user',
					'load_template': 'upload',
					'event': 'fb_import',
					'fb_id': fb_id
				},
				
				success: function(msg)
				{
				obj=jQuery.parseJSON(msg);
				
					message_box(obj.message.type, obj.message.text);
				}
			});
		
		}
	});
}

function fb_reg()
{

	FB.login(function(response) 
	{
	
		if (response.authResponse) 
		{
			FB.api('/me', function(user_date) 
			{
				
				fb_id=user_date.id;
				fb_login_name=user_date.email;
				fb_first_name=user_date.first_name;
				fb_last_name=user_date.last_name;
				
					$.ajax(
					{
						url: 'api/api_load.php',
						type: "POST",
						data: 
						{
							'load_material': 'page_user',
							'load_template': 'upload',
							'event': 'fb_reg',
							'fb_id': fb_id,
							'fb_login': fb_login_name,
							'fb_first_name': fb_first_name,
							'fb_last_name': fb_last_name
						},
						
						success: function(msg)
						{
							obj=jQuery.parseJSON(msg);
							
							if (obj.login==1)
							fb_login();
							else
							message_box(obj.message.type, obj.message.text);
						}
					});
			});
		}
	},{scope: 'email'});
}

$(document).on('click', 'span.brows-button', function(){
	moxman.browse({fields: 'image-pick'});
});
