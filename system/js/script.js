$(function()
{
	resize_window();
	
	$('span.mobile-menu-button').click(function()
	{
		$('div.mobile-menu-wrap').slideToggle(200);
	});
});

function resize_window()
{
	if (device.mobile())
		$('meta[name="viewport"]').attr('content', 'width=320, user-scalable=no');
	else if (device.tablet())
		$('meta[name="viewport"]').attr('content', 'width=1400, user-scalable=no');
	else
		$('meta[name="viewport"]').attr('content', 'width=device-width, user-scalable=no');
}
