
--
-- Структура таблицы `maxtv_sites`
--

CREATE TABLE IF NOT EXISTS `maxtv_sites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `host` varchar(255) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `building_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `pic_path` varchar(255) DEFAULT NULL,
  `h_position` int(11) NOT NULL DEFAULT '0',
  `h_color` varchar(255) DEFAULT NULL,
  `seo_title` text,
  `seo_description` text,
  `seo_keywords` text,
  `main_b_title` varchar(255) DEFAULT NULL,
  `main_b_description` text,
  `main_b_background` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `phone_activate` int(11) NOT NULL DEFAULT '1',
  `s_email` text,
  `email` varchar(255) DEFAULT NULL,
  `email_activate` int(11) NOT NULL DEFAULT '1',
  `letter_template` text,
  `inquiry_emails` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address_activate` int(11) NOT NULL DEFAULT '1',
  `address_lat` varchar(32) DEFAULT NULL,
  `address_lng` varchar(32) DEFAULT NULL,
  `form_activate` int(11) NOT NULL DEFAULT '1',
  `map_activate` int(11) NOT NULL DEFAULT '1',
  `color` varchar(255) DEFAULT NULL,
  `t_color` varchar(255) DEFAULT NULL,
  `m_color` varchar(255) DEFAULT NULL,
  `mh_color` varchar(255) DEFAULT NULL,
  `b1_title` varchar(255) DEFAULT NULL,
  `b1_description` text,
  `b2_title` varchar(255) DEFAULT NULL,
  `b2_description` text,
  `b3_title` varchar(255) DEFAULT NULL,
  `b3_descrition` text,
  `b3_background` varchar(255) DEFAULT NULL,
  `b4_title` varchar(255) DEFAULT NULL,
  `b4_description` text,
  `amenities_view` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `maxtv_sites`
--

INSERT INTO `maxtv_sites` (`id`, `host`, `user_id`, `building_id`, `title`, `pic_path`, `h_position`, `h_color`, `seo_title`, `seo_description`, `seo_keywords`, `main_b_title`, `main_b_description`, `main_b_background`, `phone`, `phone_activate`, `s_email`, `email`, `email_activate`, `letter_template`, `inquiry_emails`, `address`, `address_activate`, `address_lat`, `address_lng`, `form_activate`, `map_activate`, `color`, `t_color`, `m_color`, `mh_color`, `b1_title`, `b1_description`, `b2_title`, `b2_description`, `b3_title`, `b3_descrition`, `b3_background`, `b4_title`, `b4_description`, `amenities_view`) VALUES
(1, 'millgatemanor.maxtvmedia.com', 358, 128, 'Millgate Manor III', '', 1, 'rgba(94, 133, 229, 0.81)', 'Millgate Manor III', '', '', NULL, NULL, NULL, '(416) 621-2752', 1, 'artem@maxtvmedia.com', 'artem@maxtvmedia.com', 1, '<div style="margin: 0px; padding: 0px; float: left; width: 100%; font-family: ''Segoe UI'', Verdana, Tahoma, Arial, Helvetica, sans-serif; font-size: 12pt;">\r\n<div style="margin: 50px auto; width: 640px;">\r\n<div style="padding: 20px 30px; background: #f5f5f5;"><span style="display: block;"><span style="font-weight: bold;">Name:</span> [name]</span> <span style="display: block;"><span style="font-weight: bold;">E-mail:</span> [email]</span> <span style="display: block;"><span style="font-weight: bold;">Phone:</span> [phone]</span> <span style="display: block;"><span style="font-weight: bold;">Subject:</span> [subject]</span> <span style="display: block;"><span style="font-weight: bold;">Message:</span></span>\r\n<p style="margin: 0px;">[message]</p>\r\n</div>\r\n</div>\r\n</div>', NULL, '303 Mill Rd, Toronto, Ontario M9C 1Y5, Canada', 1, '43.6387389', '-79.58332819999998', 1, 1, 'rgba(117, 163, 255, 0.81)', 'rgb(255, 255, 255)', 'rgb(255, 255, 255)', 'rgb(0, 0, 255)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{"1":{"value":"1"},"4":{"value":"1"},"8":{"value":"1"},"2":{"value":"1"},"6":{"value":"1"},"9":{"value":"1"},"3":{"value":"1"},"7":{"value":"1"}}');

-- --------------------------------------------------------

--
-- Структура таблицы `maxtv_sites_gallery`
--

CREATE TABLE IF NOT EXISTS `maxtv_sites_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `img_description` varchar(255) DEFAULT NULL,
  `pic_path` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Дамп данных таблицы `maxtv_sites_gallery`
--

INSERT INTO `maxtv_sites_gallery` (`id`, `site_id`, `img_description`, `pic_path`, `priority`) VALUES
(14, 1, 'ddd', '/uploads/1/Millgate-Manor-820-Burnhamthorpe-3.jpg', 0),
(20, 1, '22', '/uploads/1/820-44.jpg', 0),
(21, 1, '33', '/uploads/1/slide-1.jpg', 0),
(22, 1, '44', '/uploads/1/bg-1.jpg', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `maxtv_sites_menu`
--

CREATE TABLE IF NOT EXISTS `maxtv_sites_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `block_id` varchar(255) DEFAULT NULL,
  `pic_path` text,
  `hash` text,
  `title` varchar(255) NOT NULL,
  `content` text,
  `c_title` text,
  `ct_color` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `activate` tinyint(1) NOT NULL DEFAULT '1',
  `draggable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `maxtv_sites_menu`
--

INSERT INTO `maxtv_sites_menu` (`id`, `site_id`, `block_id`, `pic_path`, `hash`, `title`, `content`, `c_title`, `ct_color`, `priority`, `activate`, `draggable`) VALUES
(1, 1, 'home', '/uploads/1/bg-1.jpg', 'a4a042cf4fd6bfb47701cbc8a1653ada', 'Home', '<p><span class="slide-label" style="color: #ffffff;">Found in Toronto''s Etobicoke area and built in 1974 by Deltan Realty Ltd</span></p>\r\n<p>&nbsp;</p>', 'Welcome to Millgate Manor III', 'rgb(255, 255, 255)', 0, 1, 0),
(2, 1, 'about-us', '', NULL, 'About Us', '<p><span style="color: #000000;">Millgate Manor III is a high-rise condo located in the Markland Woods neighbourhood. Millgate Manor III is a 22 storey condo, located at 812 Burnhamthorpe Rd. With suites ranging in size from 1240 to 1270 sqft, this Toronto condo has 215 units. Along with everything it has to offer, this is the 98th least expensive condo in Etobicoke with a price per square foot of $330.</span></p>', 'This Toronto condo sits near the intersection of Burnhamthorpe Rd & Old Burnhamthorpe Rd (West).', 'rgb(0, 0, 0)', 0, 1, 1),
(3, 1, 'gallery', '', NULL, 'Gallery', '', '', 'rgb(218, 139, 163)', 1, 1, 1),
(4, 1, 'contact-us', '', NULL, 'Contact Us', '<p>If you are interested in learning more about Milgate Manor III Condos, please fill<br /> out the form below and we will contact you.</p>', 'Contact Us', 'rgb(0, 0, 0)', 5, 1, 0),
(5, 1, 'neighbourhood', '', '274ad4786c3abca69fa097b85867d9a4', 'Neighbourhood', '<p>Recommended nearby places to eat are McDonald''s. If you can''t start your day with out caffeine fear not, your nearby choices include McDonald''s. Groceries can be found at Yi Xing Supermarket which is only a few minutes walk away and you''ll find Pharmasave a 5 minute walk as well. Entertainment options near Millgate Manor include Markland Restaurant And Pub. Nearby schools include: Silverthorn Collegiate Institute and Millwood Junior School. Love being outside? Look no further than Garnetwood Park, Mill Valley Park or Bloordale Park, which are a 5 minute walk away from this condo.</p>', 'What’s Nearby', 'rgb(255, 255, 255)', 3, 1, 1),
(12, 1, 'amenities', '', NULL, 'Amenities', '<p>Proudly Managed by Brookfield Condominium Services LTD.<br /> Management PH #: (416) 621-2752</p>', 'Amenities', 'rgb(0, 0, 0)', 2, 1, 1);


CREATE TABLE IF NOT EXISTS `maxtv_lending_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_key` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `site_link` text,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
