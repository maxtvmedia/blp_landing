-- readme
-- При добалении нового лендинга, нужно исполни ть запросы что описаны ниже.
-- Также нужно создавать папку {SITE_ID} в папке /uploads, где будут храниться все рисунки фонов, галереи и т д

-- maxtv_sites.host = {LENDING}.maxtvmedia.com 
-- maxtv_sites.user_id = {USER_ID} -- кто может редактировать сайт
-- maxtv_sites.building_id = {BUILDING_ID} - айди билдинга (аменитис тянутться по этому айди с основной базы)
-- maxtv_sites_gallery.site_id = {SITE_ID} = maxtv_sites.id
-- -----

INSERT INTO `maxtv_sites` (`host`, `user_id`, `building_id`, `title`, `pic_path`, `h_position`, `h_color`, `seo_title`, `seo_description`, `seo_keywords`, `main_b_title`, `main_b_description`, `main_b_background`, `phone`, `phone_activate`, `s_email`, `email`, `email_activate`, `letter_template`, `inquiry_emails`, `address`, `address_activate`, `address_lat`, `address_lng`, `form_activate`, `map_activate`, `color`, `t_color`, `m_color`, `mh_color`, `b1_title`, `b1_description`, `b2_title`, `b2_description`, `b3_title`, `b3_descrition`, `b3_background`, `b4_title`, `b4_description`, `amenities_view`) VALUES
('{LENDING}.maxtvmedia.com', {USER_ID}, {BUILDING_ID}, 'Site name', '', 1, 'rgba(94, 133, 229, 0.81)', '', '', '', NULL, NULL, NULL, '(999) 999-9999', 1, 'email@site.com', 'email@site.com', 1, '<div style="margin: 0px; padding: 0px; float: left; width: 100%; font-family: ''Segoe UI'', Verdana, Tahoma, Arial, Helvetica, sans-serif; font-size: 12pt;">\r\n<div style="margin: 50px auto; width: 640px;">\r\n<div style="padding: 20px 30px; background: #f5f5f5;"><span style="display: block;"><span style="font-weight: bold;">Name:</span> [name]</span> <span style="display: block;"><span style="font-weight: bold;">E-mail:</span> [email]</span> <span style="display: block;"><span style="font-weight: bold;">Phone:</span> [phone]</span> <span style="display: block;"><span style="font-weight: bold;">Subject:</span> [subject]</span> <span style="display: block;"><span style="font-weight: bold;">Message:</span></span>\r\n<p style="margin: 0px;">[message]</p>\r\n</div>\r\n</div>\r\n</div>', NULL, '303 Mill Rd, Toronto, Ontario M9C 1Y5, Canada', 1, '43.6387389', '-79.58332819999998', 1, 1, 'rgba(117, 163, 255, 0.81)', 'rgb(255, 255, 255)', 'rgb(255, 255, 255)', 'rgb(0, 0, 255)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');


INSERT INTO `maxtv_sites_gallery` (`site_id`, `img_description`, `pic_path`, `priority`) VALUES
({SITE_ID}, 'ddd', '/uploads/1.jpg', 0),
({SITE_ID}, '22', '/uploads/2.jpg', 0),
({SITE_ID}, '33', '/uploads/3.jpg', 0);

INSERT INTO `maxtv_sites_menu` (`site_id`, `block_id`, `pic_path`, `hash`, `title`, `content`, `c_title`, `ct_color`, `priority`, `activate`, `draggable`) VALUES
({SITE_ID}, 'home', '/uploads/1.jpg', 'a4a042cf4fd6bfb47701cbc8a1653ada', 'Home', '<p><span class="slide-label" style="color: #ffffff;">Main description</span></p>\r\n<p>&nbsp;</p>', 'Welcome to our building', 'rgb(255, 255, 255)', 0, 1, 0),
({SITE_ID}, 'about-us', '', NULL, 'About Us', 'About us', 'Title about us', 'rgb(0, 0, 0)', 0, 1, 1),
({SITE_ID}, 'gallery', '', NULL, 'Gallery', '', '', 'rgb(218, 139, 163)', 1, 1, 1),
({SITE_ID}, 'contact-us', '', NULL, 'Contact Us', 'Contacts text', 'Contact Us', 'rgb(0, 0, 0)', 5, 1, 0),
({SITE_ID}, 'neighbourhood', '', '274ad4786c3abca69fa097b85867d9a4', 'Neighbourhood', 'Description', 'Title', 'rgb(255, 255, 255)', 3, 1, 1),
({SITE_ID}, 'amenities', '', NULL, 'Amenities', 'Description', 'Amenities', 'rgb(0, 0, 0)', 2, 1, 1);

